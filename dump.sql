create table if not exists clients
(
    id       int auto_increment
        primary key,
    name     varchar(255) null,
    lastname varchar(255) null,
    phone    varchar(255) null,
    rating   int          null
);

create table if not exists sms_log
(
    id        int auto_increment
        primary key,
    number    varchar(255)   null,
    text      text           null,
    cost      decimal(10, 2) null,
    user_id   int            null,
    client_id int            null,
    hash      varchar(255)   null,
    status    int default 0  null
);

create table if not exists tbl_user
(
    id       int auto_increment
        primary key,
    username varchar(128) not null,
    password varchar(128) not null,
    email    varchar(128) not null
);
