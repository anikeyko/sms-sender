<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<?php
$mainMenu = array(
    array('label'=>'Главная', 'url'=>array('/site/index')),
    array('label'=>'Клиенты', 'url'=>array('/client/index')),
);
$mainMenu[] = (Yii::app()->user->isGuest)?array('label'=>'Вход', 'url'=>array('/site/login')):array('label'=>'Выйти ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'));
?>
<body>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">SMS-sender</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <?php foreach ($mainMenu as $menuItem): ?>
            <a class="p-2 text-dark" href="<?= $menuItem['url'][0] ?>"><?= $menuItem['label'] ?></a>
        <?php endforeach; ?>
    </nav>
</div>
<div class="container">
    <?php if ($this->breadcrumbs): ?>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <?php foreach ($this->breadcrumbs as $label=>$breadcrumb): ?>
                    <?php if (is_array($breadcrumb)): ?>
                        <li class="breadcrumb-item"><a href="<?= $this->createUrl($breadcrumb[0]) ?>"><?= $label ?></a></li>
                    <?php else: ?>
                        <li class="breadcrumb-item active" aria-current="page"><?= $breadcrumb ?></li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ol>
        </nav>
    <?php endif; ?>

	<?= $content; ?>

	<div class="clear"></div>

</div><!-- page -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
