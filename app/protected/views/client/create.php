<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Клиенты'=>array('index'),
	'Добавить',
);

$this->menu=array(
	array('label'=>'List Client', 'url'=>array('index')),
	array('label'=>'Manage Client', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>