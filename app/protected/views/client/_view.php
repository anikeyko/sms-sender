<?php
/* @var $this ClientController */
/* @var $data Client */
?>

<div class="view client_view">
    <div class="client_view_left">
        <b><?php echo CHtml::encode($data->getAttributeLabel('lastname')); ?>:</b>
        <?php echo CHtml::encode($data->lastname); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
        <?php echo CHtml::encode($data->name); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
        <?php echo CHtml::encode($data->phone); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('rating')); ?>:</b>
        <?php echo CHtml::encode($data->rating); ?>
    </div>
    <div class="client_view_right">
        <button type="button" class="btn btn-success smsModalTrigger" data-id="<?= $data->id ?>">Отправить анкету</button>
    </div>


</div>