<?php
/* @var $this ClientController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Клиенты',
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Manage Client', 'url'=>array('admin')),
);
?>

<a href="<?= $this->createUrl('client/create') ?>" class="btn btn-primary">Добавить</a>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
    'summaryText' => ''
)); ?>

<div class="modal fade" id="smsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Отправить анкету</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Вы действительно хотите отправить смс?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary" id="smsSend" data-id="">Отправить</button>
            </div>
        </div>
    </div>
</div>

<div class="toast" id="myToast" style="position: absolute; top: 10px; right: 10px;">
    <div class="toast-header">
        <strong class="mr-auto"><i class="fa fa-grav"></i> Уведомление</strong>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
    </div>
    <div class="toast-body">
        It's been a long time since you visited us. We've something special for you. <a href="#">Click here!</a>
    </div>
</div>

<script>
    $(document).ready(function(){
        $(document).on('click', '.smsModalTrigger', function(e) {
            var id = $(this).data('id');
            $('#smsSend').data('id', id);
            $('#smsModal').modal();
        });

        $(document).on('click', '#smsSend', function(e) {
            var id = $(this).data('id');
            $.post('<?= $this->createUrl('client/sms') ?>', {
                '<?= Yii::app()->request->csrfTokenName ?>': '<?= Yii::app()->request->csrfToken ?>',
                'id': id
            }, function(data) {
                if (data.status=='success') {
                    $('#smsModal').modal('hide');
                    $('.toast-body').text('Анкета успешно отправлена.');
                    $("#myToast").toast('show', {
                        delay: 3000
                    });
                } else {
                    $('.toast-body').text('Ошибка при отправке.');
                    $("#myToast").toast('show', {
                        delay: 3000
                    });
                }
            }, 'json');
        });
    });
</script>
