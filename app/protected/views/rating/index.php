<form action="/rating" method="post">
    <p>Выберите оценку</p>
    <div class="form-group">
        <select name="rating" id="" class="form-control">
            <?php for ($i=0; $i<=10; $i++): ?>
                <option value="<?= $i ?>"><?= $i ?></option>
            <?php endfor; ?>
        </select>
    </div>
    <input type="hidden" name="h" value="<?= $hash ?>">
    <button type="submit" class="btn btn-primary">Оценить</button>
</form>
