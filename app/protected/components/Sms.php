<?php


class Sms
{
    private $apikey = '';
    private $baseUrl = 'http://smspilot.ru/api.php';
    public $client = null;
    public $text = '';
    private $cost = 0;
    public $log = null;

    public function __construct($client, $text)
    {
        $this->client = $client;
        $this->text = $text;
        $this->apikey = Yii::app()->params['sms_apikey'];
    }

    public function send()
    {
        $out = false;
        if ($this->readyToSend()) {
            $url = '?send='.$this->text.'&to='.$this->client->phone.'&apikey='.$this->apikey.'&format=json';

            $client = new GuzzleHttp\Client([
                'base_uri'=>$this->baseUrl
            ]);

            $response = $client->request('GET', $url);

            $code = $response->getStatusCode();
            if ($code == 200) {
                $body = (string) $response->getBody();
                $data = CJSON::decode($body);
                if (!isset($data['error'])) {
                    $this->cost = $data['cost'];

                    $log = $this->log;
                    $log->status = 1;
                    $log->save();

                    $out = true;
                }
            }
        }
        return $out;
    }

    public function readyToSend()
    {
        if ($this->client && $this->client->phone && $this->text) {
            return true;
        }

        return false;
    }

}