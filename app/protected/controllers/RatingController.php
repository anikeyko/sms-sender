<?php

class RatingController extends Controller
{
	public function actionIndex()
	{
	    $this->layout = '/layouts/blank';

	    $hash = Yii::app()->request->getParam('h', null);
	    if ($sms_log = SmsLog::model()->findByAttributes(['hash'=>$hash])) {
	        if (Yii::app()->request->isPostRequest) {
	            $rating = Yii::app()->request->getParam('rating', 0);
                $rating = intval($rating);
                $rating = ($rating>10)?10:$rating;
                $rating = ($rating<0)?0:$rating;

	            $client = $sms_log->client;
	            $client->rating = $rating;
	            $client->save();
            }
            $this->render('index', compact('hash'));
        } else {
	        throw new CHttpException(404, 'Not found');
        }
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}