<?php

/**
 * This is the model class for table "sms_log".
 *
 * The followings are the available columns in table 'sms_log':
 * @property integer $id
 * @property string $number
 * @property string $text
 * @property string $cost
 * @property integer $user_id
 * @property integer $client_id
 * @property string $hash
 * @property integer $status
 */
class SmsLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sms_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, client_id, status', 'numerical', 'integerOnly'=>true),
			array('number', 'length', 'max'=>255),
			array('cost', 'length', 'max'=>10),
			array('text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, number, text, cost, user_id, client_id, hash, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'client' => [self::BELONGS_TO, 'Client', 'client_id']
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'number' => 'Number',
			'text' => 'Text',
			'cost' => 'Cost',
			'user_id' => 'User',
			'client_id' => 'Client',
			'hash' => 'Hash',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('hash',$this->hash);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SmsLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
